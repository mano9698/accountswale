<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UI\HomeController@home');

Route::get('/post_jobs', 'UI\HomeController@post_jobs');
Route::post('/AddFreeJobs', 'UI\HomeController@AddFreeJobs');

Route::post('/apply_jobs', 'UI\HomeController@apply_jobs');

Route::get('/become_partner', 'UI\HomeController@become_partner');

Route::post('/AddFranchisee', 'UI\HomeController@AddFranchisee');

Route::group(['prefix' => '/admin'], function () {

    Route::get('/login', 'Admin\AuthendicationController@login');

    Route::post('/login_check', 'Admin\AuthendicationController@admin_login');

    Route::get('/dashboard', 'Admin\HomeController@dashboard');

    Route::get('/change_password', 'Admin\HomeController@change_password');

    Route::post('/update_password', 'Admin\HomeController@update_password');

    Route::get('/jobs_list', 'Admin\HomeController@jobs_list');

    Route::get('/add_videos', 'Admin\VideosController@add_videos');

    Route::get('/videos_list', 'Admin\VideosController@videos_list');

    Route::get('/edit_videos/{id}', 'Admin\VideosController@edit_videos');

    Route::get('/delete_videos/{id}', 'Admin\VideosController@delete_videos');

    Route::get('/candidates_list/{id}', 'Admin\HomeController@candidates_list');

    Route::get('/delete_jobs/{id}', 'Admin\HomeController@delete_jobs');

    Route::get('/franchisee_list', 'Admin\HomeController@franchisee_list');

    Route::post('/store_videos', 'Admin\VideosController@store_videos');

    Route::post('/update_videos', 'Admin\VideosController@update_videos');

    Route::post('/changeStatus', 'Admin\VideosController@changeStatus');

    Route::post('/changejobstatus', 'Admin\HomeController@changejobstatus');

    Route::get('/logout', 'Admin\HomeController@admin_logout');
});


Route::group(['prefix' => '/income_tax'], function () {

    Route::get('/list', 'Admin\ITRTaxDeclaration@income_tax_list');

    Route::get('/add_income_tax', 'Admin\ITRTaxDeclaration@add_income_tax');

    Route::post('/store_income_tax', 'Admin\ITRTaxDeclaration@store_income_tax');

    Route::post('/update_income_tax', 'Admin\ITRTaxDeclaration@update_income_tax');

    Route::get('/edit_income_tax/{id}', 'Admin\ITRTaxDeclaration@edit_income_tax');

    Route::get('/delete_income_tax/{id}', 'Admin\ITRTaxDeclaration@delete_income_tax');

    Route::get('/delete_income_tax_lists/{id}', 'Admin\ITRTaxDeclaration@delete_income_tax_lists');
});

Route::group(['prefix' => '/payslip'], function () {

    Route::get('/list', 'Admin\ITRTaxDeclaration@payslip_list');

    Route::get('/new_payslip', 'Admin\ITRTaxDeclaration@new_payslip');

    Route::post('/store_payslip', 'Admin\ITRTaxDeclaration@store_payslip');

    Route::post('/update_payslip', 'Admin\ITRTaxDeclaration@update_payslip');

    Route::get('/edit_payslip/{id}', 'Admin\ITRTaxDeclaration@edit_payslip');

    Route::get('/delete_payslip/{id}', 'Admin\ITRTaxDeclaration@delete_payslip');
});

Route::group(['prefix' => '/form16'], function () {

    Route::get('/list', 'Admin\ITRTaxDeclaration@form16_list');

    Route::get('/new_form16', 'Admin\ITRTaxDeclaration@new_form16');

    Route::post('/store_form16', 'Admin\ITRTaxDeclaration@store_form16');

    Route::post('/update_form16', 'Admin\ITRTaxDeclaration@update_form16');

    Route::get('/edit_form16/{id}', 'Admin\ITRTaxDeclaration@edit_form16');

    Route::get('/delete_form16/{id}', 'Admin\ITRTaxDeclaration@delete_form16');
});


Route::group(['prefix' => '/investment'], function () {

    Route::get('/list', 'Admin\ITRTaxDeclaration@investment_list');

    Route::get('/new_investment', 'Admin\ITRTaxDeclaration@new_investment');

    Route::post('/store_investment', 'Admin\ITRTaxDeclaration@store_investment');

    Route::post('/update_investment', 'Admin\ITRTaxDeclaration@update_investment');

    Route::get('/edit_investment/{id}', 'Admin\ITRTaxDeclaration@edit_investment');

    Route::get('/delete_investment/{id}', 'Admin\ITRTaxDeclaration@delete_investment');
});

Route::group(['prefix' => '/itr'], function () {

    Route::get('/list', 'Admin\ITRTaxDeclaration@itr_list');

    Route::get('/new_itr', 'Admin\ITRTaxDeclaration@new_itr');

    Route::post('/store_itr', 'Admin\ITRTaxDeclaration@store_itr');

    Route::post('/update_itr', 'Admin\ITRTaxDeclaration@update_itr');

    Route::get('/edit_itr/{id}', 'Admin\ITRTaxDeclaration@edit_itr');

    Route::get('/delete_itr/{id}', 'Admin\ITRTaxDeclaration@delete_itr');
});


Route::group(['prefix' => '/users'], function () {

    Route::get('/dashboard', 'Admin\HomeController@user_dashboard');

    Route::get('/list', 'Admin\HomeController@users_list');

    Route::get('/new_user', 'Admin\HomeController@new_user');

    Route::get('/logout', 'Admin\HomeController@user_logout');

    Route::post('/store_user', 'Admin\HomeController@store_user');

    Route::post('/changeStatus', 'Admin\HomeController@changeStatus');

    Route::get('/change_password', 'Admin\HomeController@change_password');

    Route::post('/update_password', 'Admin\HomeController@update_password');
});



Route::group(['prefix' => '/blogs'], function () {

    Route::get('/list', 'UI\ForumController@blog_list');

    Route::get('/details/{id}/{slug}', 'UI\ForumController@blog_details');

});
