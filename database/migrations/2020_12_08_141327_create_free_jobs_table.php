<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreeJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('jobs_id')->nullable();
            $table->string('employer_name');
            $table->string('job_title');
            $table->string('slug')->nullable();
            $table->string('contact');
            $table->string('email');
            $table->string('location');
            $table->string('work_from_home');
            $table->string('qualification');
            $table->string('experience');
            $table->string('salary_budget');
            $table->longText('job_description');
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_jobs');
    }
}
