<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomeTaxDeclarationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_tax_declaration', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('year');
            $table->string('payslip')->nullable();
            $table->string('form16')->nullable();
            $table->string('investment')->nullable();
            $table->string('amount')->nullable();
            $table->string('proof')->nullable();
            $table->string('itr')->nullable();
            $table->boolean('type')->nullable();
            $table->boolean('file_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_tax_declaration');
    }
}
