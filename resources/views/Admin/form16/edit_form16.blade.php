@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit Form16</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Edit Form16            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <!--<div class="title"><strong>Update Your Profile</strong></div>-->
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/form16/update_form16" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Financial Year</label>
                    <input type="hidden" name="id" class="form-control" value="{{ $ItrTax->id }}" placeholder="Paste Link "/>
                    <div class="col-sm-6">
                        <input type="hidden" class="form-control" name="id" value="{{$ItrTax->id}}">
                        <select name="year" class="form-control" id="">
                            <option value="2018-19"  @if($ItrTax->year == "2018-19") selected @endif>2018-19</option>
                            <option value="2019-20" @if($ItrTax->year == "2019-20") selected @endif>2019-20</option>
                            <option value="2020-21" @if($ItrTax->year == "2020-21") selected @endif>2020-21</option>
                            <option value="2021-22" @if($ItrTax->year == "2021-22") selected @endif>2021-22</option>
                            <option value="2022-23" @if($ItrTax->year == "2022-23") selected @endif>2022-23</option>
                        </select>
                    </div>
                  </div>
                  @if(Auth::guard('super_admin')->check())
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Users</label>
                    <div class="col-sm-6">
                        <select name="user_id" class="form-control" id="">
                            <option value="" selected disabled>Select User</option>
                            @foreach($Users as $User)
                                <option value="{{ $User->id }}" @if($ItrTax->user_id == $User->id) selected @endif>{{ $User->first_name }} {{ $User->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  @endif
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Type</label>
                    <div class="col-sm-6">
                        <select name="file_type" onchange="choose_file_type()" class="form-control" id="file_type">
                            <option selected disabled>Select Option</option>
                            <option value="1" @if($ItrTax->file_type == 1) selected @endif>File</option>
                            <option value="2" @if($ItrTax->file_type == 2) selected @endif>Paste link</option>
                        </select>
                    </div>
                  </div>
                  <div class="line"></div>

                    <div class="form-group fieldGroup" id="link" @if($ItrTax->file_type == 1) style="display: none;" @endif>
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="text" name="links" class="form-control" value="{{ $ItrTax->proof }}" placeholder="Paste Link "/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group fieldGroup" id="upload_file" @if($ItrTax->file_type == 2) style="display: none;" @endif>
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="file" name="files" class="form-control" value="{{ $ItrTax->proof }}" placeholder="Upload File "/>
                            </div>
                        </div>
                    </div>

                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-12 ml-auto">
                      {{-- <button type="submit" class="btn btn-secondary">Cancel</button> --}}
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
      <script>
          function choose_file_type(){
    var file_type = $("#file_type").val();


    if(file_type == 1){
        $("#upload_file").show();
        $("#link").hide();
    }else{
        $("#upload_file").hide();
        $("#link").show();
    }
}
       </script>
  @endsection
