@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit Payslip</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Edit Payslip            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <!--<div class="title"><strong>Update Your Profile</strong></div>-->
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/payslip/update_payslip" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Month</label>
                    <input type="hidden" name="id" class="form-control" value="{{ $ItrTax->id }}" placeholder="Paste Link "/>
                    <div class="col-sm-6">
                        <select name="year" class="form-control" id="">
                            <option value='January' @if($ItrTax->year == "January") selected @endif>January</option>
                            <option value='February' @if($ItrTax->year == "February") selected @endif>February</option>
                            <option value='March' @if($ItrTax->year == "March") selected @endif>March</option>
                            <option value='April' @if($ItrTax->year == "April") selected @endif>April</option>
                            <option value='May' @if($ItrTax->year == "May") selected @endif>May</option>
                            <option value='June' @if($ItrTax->year == "June") selected @endif>June</option>
                            <option value='July' @if($ItrTax->year == "July") selected @endif>July</option>
                            <option value='August' @if($ItrTax->year == "August") selected @endif>August</option>
                            <option value='September' @if($ItrTax->year == "September") selected @endif>September</option>
                            <option value='October' @if($ItrTax->year == "October") selected @endif>October</option>
                            <option value='November' @if($ItrTax->year == "November") selected @endif>November</option>
                            <option value='December' @if($ItrTax->year == "December") selected @endif>December</option>
                        </select>
                    </div>
                  </div>
                  @if(Auth::guard('super_admin')->check())
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Users</label>
                    <div class="col-sm-6">
                        <select name="user_id" class="form-control" id="">
                            <option value="" selected disabled>Select User</option>
                            @foreach($Users as $User)
                                <option value="{{ $User->id }}" @if($ItrTax->user_id == $User->id) selected @endif>{{ $User->first_name }} {{ $User->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  @endif
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Type</label>
                    <div class="col-sm-6">
                        <select name="file_type" onchange="choose_file_type()" class="form-control" id="file_type">
                            <option selected disabled>Select Option</option>
                            <option value="1" @if($ItrTax->file_type == 1) selected @endif>File</option>
                            <option value="2" @if($ItrTax->file_type == 2) selected @endif>Paste link</option>
                        </select>
                    </div>
                  </div>
                  <div class="line"></div>

                    <div class="form-group fieldGroup" id="link" @if($ItrTax->file_type == 1) style="display: none;" @endif>
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="text" name="links" class="form-control" value="{{ $ItrTax->proof }}" placeholder="Paste Link "/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group fieldGroup" id="upload_file" @if($ItrTax->file_type == 2) style="display: none;" @endif>
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="file" name="files" class="form-control" value="{{ $ItrTax->proof }}" placeholder="Upload File "/>
                            </div>
                        </div>
                    </div>

                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-12 ml-auto">
                      {{-- <button type="submit" class="btn btn-secondary">Cancel</button> --}}
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
      <script>
          function choose_file_type(){
    var file_type = $("#file_type").val();


    if(file_type == 1){
        $("#upload_file").show();
        $("#link").hide();
    }else{
        $("#upload_file").hide();
        $("#link").show();
    }
}
       </script>
  @endsection
