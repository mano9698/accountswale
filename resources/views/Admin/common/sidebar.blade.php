@if(Auth::guard('super_admin')->check())
<nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      {{-- <div class="avatar">
        <img src="/Admin/freelancer/profile_pic/{{Session::get('AdminProfilePic')}}" alt="..." class="img-fluid rounded-circle">
      </div> --}}
      <div class="title">
        {{-- <h1 class="h5">{{Session::get('AdminName')}}</h1> --}}
        <h5>
                Accountswale Admin
        </h5>
      </div>
    </div>
    <!-- Sidebar Navidation Menus-->
    <ul class="list-unstyled">
            <li><a href="/admin/dashboard"> <i class="icon-home"></i>Home </a></li>

            <li><a href="#users" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Users</a>
                <ul id="users" class="collapse list-unstyled ">
                  <li><a href="/users/new_user" class="clr-white">Add Users</a></li>
                  <li><a href="/users/list" class="clr-white">Users List</a></li>
                </ul>
              </li>

            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Videos</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="/admin/add_videos" class="clr-white">Add New Videos</a></li>
                <li><a href="/admin/videos_list" class="clr-white">Videos List</a></li>
              </ul>
            </li>
            <li><a href="/admin/jobs_list"> <i class="icon-grid"></i>Jobs</a></li>
            <li><a href="/admin/franchisee_list"> <i class="icon-grid"></i>Franchisee Users</a></li>
            <li><a href="#income_tax" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Income Tax Declaration</a>
                <ul id="income_tax" class="collapse list-unstyled ">
                  <li><a href="/income_tax/add_income_tax" class="clr-white">Add New Income tax Declaration</a></li>
                  <li><a href="/income_tax/list" class="clr-white">Income Tax Declaration List</a></li>
                </ul>
              </li>

              <li><a href="#payslips" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Payslips</a>
                <ul id="payslips" class="collapse list-unstyled ">
                  <li><a href="/payslip/new_payslip" class="clr-white">Add New Payslips</a></li>
                  <li><a href="/payslip/list" class="clr-white">Payslips List</a></li>
                </ul>
              </li>

              <li><a href="#form16" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Form 16</a>
                <ul id="form16" class="collapse list-unstyled ">
                  <li><a href="/form16/new_form16" class="clr-white">Add Form 16</a></li>
                  <li><a href="/form16/list" class="clr-white">Form 16 List</a></li>
                </ul>
              </li>

              <li><a href="#investment" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Investment Proofs</a>
                <ul id="investment" class="collapse list-unstyled ">
                  <li><a href="/investment/new_investment" class="clr-white">Add Investment Proofs</a></li>
                  <li><a href="/investment/list" class="clr-white">Investment Proofs List</a></li>
                </ul>
              </li>

              <li><a href="#itr" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>ITR </a>
                <ul id="itr" class="collapse list-unstyled ">
                  <li><a href="/itr/new_itr" class="clr-white">Add ITR Proofs</a></li>
                  <li><a href="/itr/list" class="clr-white">ITR List</a></li>
                </ul>
              </li>


            {{-- <li><a href="/admin/change_password"> <i class="fa fa-key"></i>Change Password</a></li> --}}

            <li><a href="/admin/logout"> <i class="icon-logout"></i>Log out </a></li>
    </ul>

  </nav>
@elseif(Auth::guard('user')->check())

<nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      <div class="title">
        <h1 class="h5"> {{ Session::get('UserFname') }} {{ Session::get('UserLname') }}</h1>
      </div>
    </div>
    <!-- Sidebar Navidation Menus-->
    <ul class="list-unstyled">
            <li class="active"><a href="/users/dashboard"> <i class="icon-home"></i>Home </a></li>
<!--<li><a href="works.html"> <i class="icon-grid"></i>Lead tracker</a></li>-->
            {{-- <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Lead tracker</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="/admin/freelancer/request_call_back_lists">Request A Callback</a></li>
                <li><a href="/admin/freelancer/get_quote_lists">Get a Quote</a></li>
              </ul>
            </li> --}}

            <li><a href="#income_tax" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Income Tax Declaration</a>
                <ul id="income_tax" class="collapse list-unstyled ">
                  <li><a href="/income_tax/add_income_tax" class="clr-white">Add New Income tax Declaration</a></li>
                  <li><a href="/income_tax/list" class="clr-white">Income Tax Declaration List</a></li>
                </ul>
              </li>

              <li><a href="#payslips" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Payslips</a>
                <ul id="payslips" class="collapse list-unstyled ">
                  <li><a href="/payslip/new_payslip" class="clr-white">Add New Payslips</a></li>
                  <li><a href="/payslip/list" class="clr-white">Payslips List</a></li>
                </ul>
              </li>

              <li><a href="#form16" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Form 16</a>
                <ul id="form16" class="collapse list-unstyled ">
                  <li><a href="/form16/new_form16" class="clr-white">Add Form 16</a></li>
                  <li><a href="/form16/list" class="clr-white">Form 16 List</a></li>
                </ul>
              </li>

              <li><a href="#investment" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Investment Proofs</a>
                <ul id="investment" class="collapse list-unstyled ">
                  <li><a href="/investment/new_investment" class="clr-white">Add Investment Proofs</a></li>
                  <li><a href="/investment/list" class="clr-white">Investment Proofs List</a></li>
                </ul>
              </li>

              <li><a href="#itr" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>ITR </a>
                <ul id="itr" class="collapse list-unstyled ">
                  <li><a href="/itr/new_itr" class="clr-white">Add ITR Proofs</a></li>
                  <li><a href="/itr/list" class="clr-white">ITR List</a></li>
                </ul>
              </li>

            <li><a href="/users/change_password"> <i class="fa fa-key"></i>Change Password</a></li>

            <li><a href="/admin/freelancer/user_logout"> <i class="icon-logout"></i>Log out </a></li>
    </ul>

  </nav>
  @endif
