@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Jobs list</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Jobs List            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              @if(session('message'))
              <div class="alert alert-success width100">
                  <ul>
                      <li>{!! session('message') !!}</li>
                  </ul>
              </div>
          @endif
              <div class="table-responsive">
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Employer Name</th>
                      <th>Email Id</th>
                      <th>Contact Number</th>
                      <th>Job Title</th>
                      <th>Location</th>
                      <th>Qualification</th>
                      <th>Experience</th>
                      <th>Salary Budget</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($FreeJobs)
                      @foreach($FreeJobs as $Jobs)
                    <tr>
                        <td></td>
                      <!--<td><div class="avatar"> <img src="../img/avatar-1.jpg" alt="..." class="img-fluid"></div><a href="#" class="name"></td>-->
                      <td><strong class="d-block">{{$Jobs->employer_name}}</strong></td>
                      <td>{{$Jobs->email}}</td>
                      <td>{{$Jobs->contact}}</td>
                      <td>{{$Jobs->job_title}}</td>
                      <td>{{$Jobs->location}}</td>
                      <td>{{$Jobs->qualification}}</td>
                      <td>{{$Jobs->experience}}</td>
                      <td>{{$Jobs->salary_budget}}</td>
                      <td>
                        <input data-id="{{$Jobs->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $Jobs->status ? 'checked' : '' }}>
                     </td>
                      <td>
                        <a href="/admin/candidates_list/{{$Jobs->id}}" class="btn button-sm blue" target="_blank">Check Applied Candidates</a>
                        <a href="/admin/delete_jobs/{{$Jobs->id}}" class="btn button-sm blue" onclick="return confirm(' Are you sure. You want to delete?');">Delete</a>
                     </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
  <script>
      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });




      $(function() {
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var id = $(this).data('id');
             console.log(status);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/admin/changejobstatus',
                data: {'status': status, 'id': id},
                success: function(data){
                  console.log(data.success)
                  alert(data.success);
                }
            });
        })
      })
    </script>
  @endsection
