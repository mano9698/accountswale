@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Dashboard</h2>
      </div>
    </div>
    {{-- <section class="no-padding-top no-padding-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="icon-user-1"></i></div><strong>New Freelancers</strong>
                </div>
                <div class="number dashtext-1">27</div>
              </div>
              <div class="progress progress-template">
                <div role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-1"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="icon-contract"></i></div><strong>New Projects</strong>
                </div>
                <div class="number dashtext-2">375</div>
              </div>
              <div class="progress progress-template">
                <div role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-2"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="icon-paper-and-pencil"></i></div><strong>New Invoices</strong>
                </div>
                <div class="number dashtext-3">140</div>
              </div>
              <div class="progress progress-template">
                <div role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-3"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="icon-writing-whiteboard"></i></div><strong>All Projects</strong>
                </div>
                <div class="number dashtext-4">41</div>
              </div>
              <div class="progress progress-template">
                <div role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-4"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<section class="margin-bottom-sm">
      <div class="container-fluid">
        <div class="row d-flex align-items-stretch">
          <div class="col-lg-4">
            <div class="stats-with-chart-1 block">
              <div class="title"> <strong class="d-block">Pending Payments</strong><span class="d-block">Lorem ipsum dolor sit</span></div>
              <div class="row d-flex align-items-end justify-content-between">
                <div class="col-5">
                  <div class="text"><strong class="d-block dashtext-3">Rs.5000</strong><span class="d-block">Oct 2020</span><small class="d-block">20 Accounts</small></div>
                </div>
                <div class="col-7">
                  <div class="bar-chart chart">
                    <canvas id="salesBarChart1"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="stats-with-chart-1 block">
              <div class="title"> <strong class="d-block">Received Payments</strong><span class="d-block">Lorem ipsum dolor sit</span></div>
              <div class="row d-flex align-items-end justify-content-between">
                <div class="col-4">
                  <div class="text"><strong class="d-block dashtext-1">Rs.20000</strong><span class="d-block">Oct 2020</span><small class="d-block">114 Accounts</small></div>
                </div>
                <div class="col-8">
                  <div class="bar-chart chart">
                    <canvas id="visitPieChart"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="stats-with-chart-1 block">
              <div class="title"> <strong class="d-block">Total requests</strong><span class="d-block">Lorem ipsum dolor sit</span></div>
              <div class="row d-flex align-items-end justify-content-between">
                <div class="col-5">
                  <div class="text"><strong class="d-block dashtext-2">134</strong><span class="d-block">Oct 2020</span><small class="d-block">+35 Accounts</small></div>
                </div>
                <div class="col-7">
                  <div class="bar-chart chart">
                    <canvas id="salesBarChart2"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="no-padding-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="checklist-block block">
              <div class="title"><strong>To Do List</strong></div>
              <div class="checklist">
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-1" name="input-1" class="checkbox-template">
                  <label for="input-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-2" name="input-2" checked class="checkbox-template">
                  <label for="input-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-3" name="input-3" class="checkbox-template">
                  <label for="input-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-4" name="input-4" class="checkbox-template">
                  <label for="input-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-5" name="input-5" class="checkbox-template">
                  <label for="input-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-6" name="input-6" class="checkbox-template">
                  <label for="input-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="messages-block block">
              <div class="title"><strong>New Messages</strong></div>
              <div class="messages"><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="img/avatar-3.jpg" alt="..." class="img-fluid">
                    <div class="status online"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Nadia Halsey</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">9:30am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="img/avatar-2.jpg" alt="..." class="img-fluid">
                    <div class="status away"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Peter Ramsy</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">7:40am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="img/avatar-1.jpg" alt="..." class="img-fluid">
                    <div class="status busy"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Sam Kaheil</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">6:55am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="img/avatar-5.jpg" alt="..." class="img-fluid">
                    <div class="status offline"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Sara Wood</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">10:30pm</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="img/avatar-1.jpg" alt="..." class="img-fluid">
                    <div class="status online"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Nader Magdy</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">9:47pm</small></div></a></div>
            </div>
          </div>
        </div>
      </div>
    </section> --}}

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
@endsection
