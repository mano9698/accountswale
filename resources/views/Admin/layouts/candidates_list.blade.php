@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Candidates list</h2>
      </div>
    </div>
    
    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Candidates List            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              
              <div class="table-responsive"> 
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email Id</th>
                      <th>Contact Number</th>
                      <th>Country</th>
                      <th>State</th>
                      <th>City</th>
                      <th>Area</th>
                      <th>Pincode</th>
                      <th>Resume</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($AppliedCandidates)
                      @foreach($AppliedCandidates as $Candidates)
                    <tr>
                        <th scope="row">{{$Candidates->id}}</th>
                      <!--<td><div class="avatar"> <img src="../img/avatar-1.jpg" alt="..." class="img-fluid"></div><a href="#" class="name"></td>-->
                      <td><strong class="d-block">{{$Candidates->first_name}}</strong></td>
                      <td>{{$Candidates->last_name}}</td>
                      <td>{{$Candidates->email}}</td>
                      <td>{{$Candidates->contact}}</td>
                      <td>{{$Candidates->country}}</td>
                      <td>{{$Candidates->state}}</td>
                      <td>{{$Candidates->city}}</td>
                      <td>{{$Candidates->area}}</td>
                      <td>{{$Candidates->pincode}}</td>
                      <td><a href="/UI/resume/{{$Candidates->resume}}">Download CV</a></td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
  <script>
      $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
    
  
  
      $(function() {
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
             console.log(status);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '/admin/changejobstatus',
                data: {'status': status, 'id': id},
                success: function(data){
                  console.log(data.success)
                  alert(data.success);
                }
            });
        })
      })
    </script>
  @endsection