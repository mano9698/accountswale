@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Add Income Tax Declaration</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Add Income Tax Declaration           </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <!--<div class="title"><strong>Update Your Profile</strong></div>-->
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/income_tax/store_income_tax" method="post" >
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Financial Year</label>
                    <div class="col-sm-6">
                        <select name="year" class="form-control" id="">
                            <option value="" selected disabled>Select Financial Year</option>
                            <option value="2018-19">2018-19</option>
                            <option value="2019-20">2019-20</option>
                            <option value="2020-21">2020-21</option>
                            <option value="2021-22">2021-22</option>
                            <option value="2022-23">2022-23</option>
                        </select>
                    </div>
                  </div>
                  @if(Auth::guard('super_admin')->check())
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Users</label>
                    <div class="col-sm-6">
                        <select name="user_id" class="form-control" id="">
                            <option value="" selected disabled>Select User</option>
                            @foreach($Users as $User)
                                <option value="{{ $User->id }}">{{ $User->first_name }} {{ $User->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  @endif
                  <div class="line"></div>

                    <div class="form-group fieldGroup">
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="text" name="investment[]" class="form-control" placeholder="Enter investment "/>
                            </div>

                            <div class="col-sm-4">
                                <input type="text" name="amount[]" class="form-control" placeholder="Enter amount "/>
                            </div>

                            <div class="input-group-addon">
                                <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add</a>
                            </div>
                        </div>
                    </div>

                <!-- copy of input fields group -->
                <div class="form-group fieldGroupCopy" style="display: none;">
                    <div class="input-group">
                        <div class="col-sm-4">
                            <input type="text" name="investment[]" class="form-control" placeholder="Enter investment "/>
                        </div>

                        <div class="col-sm-4">
                            <input type="text" name="amount[]" class="form-control" placeholder="Enter amount "/>
                        </div>
                        <div class="input-group-addon">
                            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
                        </div>
                    </div>
                </div>


                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-12 ml-auto">
                      {{-- <button type="submit" class="btn btn-secondary">Cancel</button> --}}
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
    <script>
        $(document).ready(function(){
    //group add limit
    var maxGroup = 10;

    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });

    //remove fields group
    $("body").on("click",".remove",function(){
        $(this).parents(".fieldGroup").remove();
    });
});
    </script>
  @endsection
