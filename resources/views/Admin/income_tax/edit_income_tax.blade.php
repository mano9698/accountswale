@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Add Income Tax Declaration</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Add Income Tax Declaration          </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <!--<div class="title"><strong>Update Your Profile</strong></div>-->
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/income_tax/update_income_tax" method="post" >
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Financial Year</label>
                    <div class="col-sm-6">
                        <input type="hidden" class="form-control" name="id" value="{{$ItrTax->id}}">
                        <select name="year" class="form-control" id="">
                            <option value="2018-19"  @if($ItrTax->year == "2018-19") selected @endif>2018-19</option>
                            <option value="2019-20" @if($ItrTax->year == "2019-20") selected @endif>2019-20</option>
                            <option value="2020-21" @if($ItrTax->year == "2020-21") selected @endif>2020-21</option>
                            <option value="2021-22" @if($ItrTax->year == "2021-22") selected @endif>2021-22</option>
                            <option value="2022-23" @if($ItrTax->year == "2022-23") selected @endif>2022-23</option>
                        </select>
                    </div>
                  </div>
                  @if(Auth::guard('super_admin')->check())
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Select Users</label>
                    <div class="col-sm-6">
                        <select name="user_id" class="form-control" id="">
                            <option value="" selected disabled>Select User</option>
                            @foreach($Users as $User)
                                <option value="{{ $User->id }}" @if($ItrTax->user_id == $User->id) selected @endif>{{ $User->first_name }} {{ $User->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  @endif
                  <div class="line"></div>

                    {{-- <div class="form-group fieldGroup">
                        <div class="input-group">
                            <div class="col-sm-4">
                                <input type="text" name="investment[]" class="form-control" placeholder="Enter investment "/>
                            </div>

                            <div class="col-sm-4">
                                <input type="text" name="amount[]" class="form-control" placeholder="Enter amount "/>
                            </div>

                            <div class="input-group-addon">
                                <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add</a>
                            </div>
                        </div>
                    </div> --}}

                    <?php
                        $income_tax_amounts = DB::table('income_tax_amounts')
                                                ->where('income_tax_amounts.income_tax_id', '=', $ItrTax->id)
                                                ->get();

                                                // echo json_encode($customer_ledger_amounts);
                                                // exit;

                ?>
                <!-- copy of input fields group -->
                @foreach($income_tax_amounts as $income)
                <div class="fieldGroup row form-width">
                    <div class="input-group">
                        <div class="col-sm-4">
                            <input type="text" name="investment[]" class="form-control" value="{{$income->investment}}" placeholder="Enter investment "/>
                        </div>

                        <div class="col-sm-4">
                            <input type="text" name="amount[]" class="form-control" value="{{$income->amount}}" placeholder="Enter amount "/>
                        </div>
                        <div class="input-group-addon">
                            <a href="/income_tax/delete_income_tax/{{$income->id}}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
                        </div>
                    </div>
                </div>
                <br>
                @endforeach

                <div class="input-group-addon">
                    <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add More</a>
                </div>

                <div class="fieldGroupCopy" style="display: none;">
                    <br>
                    <div class="input-group">
                        <div class="col-sm-4">
                            <input type="text" name="investment[]" class="form-control" placeholder="Enter investment "/>
                        </div>

                        <div class="col-sm-4">
                            <input type="text" name="amount[]" class="form-control" placeholder="Enter amount "/>
                        </div>
                        <div class="input-group-addon">
                            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
                        </div>
                    </div>
                </div>

                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-12 ml-auto">
                      {{-- <button type="submit" class="btn btn-secondary">Cancel</button> --}}
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
    <script>
        $(document).ready(function(){
    //group add limit
    var maxGroup = 10;

    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });

    //remove fields group
    $("body").on("click",".remove",function(){
        $(this).parents(".fieldGroup").remove();
    });
});
    </script>
  @endsection
