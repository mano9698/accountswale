@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Investment Proof list</h2>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Investment Proof List            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">

              <div class="table-responsive">
                @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Financial Year</th>
                      <th>Investment</th>
                      <th>Amount</th>
                      <th>Download</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($ItrTax)
                      @foreach($ItrTax as $Tax)
                    <tr>
                        <td></td>
                      <!--<td><div class="avatar"> <img src="../img/avatar-1.jpg" alt="..." class="img-fluid"></div><a href="#" class="name"></td>-->
                      <td><strong class="d-block">{{$Tax->year}}</strong></td>
                      <td>{{$Tax->investment}}</td>
                      <td>{{$Tax->amount}}</td>
                      <td>
                          @if($Tax->file_type == 1)
                            <a href="UI/investment/{{$Tax->proof}}" download>Download</a>
                            @elseif($Tax->file_type == 2)
                            <a href="{{$Tax->proof}}" target="_blank" download>Download</a>
                            @endif
                      </td>
                      <td>{{date('m-d-Y', strtotime($Tax->created_at))}}</td>
                      <td>
                        <a href="/investment/edit_investment/{{$Tax->id}}" target="_blank" class="btn button-sm blue">Edit</a>
                        <a href="/investment/delete_investment/{{$Tax->id}}" class="btn button-sm blue" onclick="return confirm(' Are you sure. You want to delete?');">Delete</a>
                     </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Accountswale. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection

