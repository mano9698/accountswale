@extends('UI.base')
@section('Content')
<section id="slider" class="slider-element slider-parallax"  data-height-xl="500" data-height-lg="400" data-height-md="300" data-height-sm="200" data-height-xs="150">
    <div class="fslider" data-arrows="true" data-pagi="false" data-lightbox="gallery">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide"><img class="image_fade" src="{{URL::asset('UI/images/landing1.jpg')}}" alt="Standard Post with Gallery"></div>
                                            <div class="slide"><img class="image_fade" src="{{URL::asset('UI/images/landing1.jpg')}}" alt="Standard Post with Gallery"></div>
                                            <div class="slide"><img class="image_fade" src="{{URL::asset('UI/images/landing1.jpg')}}" alt="Standard Post with Gallery"></div>
                                            <div class="slide"><img class="image_fade" src="{{URL::asset('UI/images/landing1.jpg')}}" alt="Standard Post with Gallery"></div>
                                        </div>
                                    </div>
                                </div>
    </section>

        <!-- Content
		============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="col_one_third nobottommargin">
                        <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                            <!--<div class="fbox-icon">
								<a href="#"><i class="i-alt noborder icon-shop"></i></a>
							</div>-->
                            <h3>Starting &amp; Managing Your Business has never been easier!<span class="subtitle">Contact us now for  end to end Company or Business Registration in India and setting up Accounting /Audit Team</span></h3>
                            <a href="#" class="button button-border button-border-thin button-palegreen"><i class="icon-phone-sign"></i> 91-86184 14801</a>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin feature_call">
                        <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">

                            <h3>Automated All-in-One Accounting Solutions
                                <!--<span class="subtitle">*Terms And Conditions apply</span>--></h3>
                            <a href="#" class="button button-3d button-large button-rounded button-orange"><i class="icon-phone"></i>  91-86184 14801</a>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin col_last">
                        <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                            <h3>Expert Assisted GST Returns Filing <i>@ 499</i><span class="subtitle">File error free GST Returns to avoid any complications and penalty .Connect our GST Experts Now</span></h3>
                            <a href="#" class="button button-border button-border-thin button-palegreen"><i class="icon-phone-sign"></i> 91-76397 59818</a>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <div class="col_one_third nobottommargin mt-5">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
							<!--<div class="fbox-icon">
								<a href="#"><i class="i-alt noborder icon-shop"></i></a>
							</div>-->
							<h3>End to end Payroll ,PF,ESI and Labour Department Consulting work<span class="subtitle">Struggling with managing employees Payroll, PF, ESI related compliance . Contact us today and be assured with best services.</span></h3>
							<a href="#" class="button button-border button-border-thin button-palegreen"><i class="icon-phone-sign"></i> 91-86184 14801</a>
						</div>
					</div>

					<div class="col_one_third nobottommargin feature_call mt-5">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">

							<h3>Virtual Office setup and communication address anywhere in India<!--<span class="subtitle">*Terms And Conditions apply</span>--></h3>
							<a href="#" class="button button-3d button-large button-rounded button-orange"><i class="icon-phone"></i> 91-86184 14801</a>
						</div>
					</div>



					<div class="col_one_third nobottommargin col_last mt-5">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
							<h3>Personal Income Tax Planning, Investment Declarations and ITR Filing <i>@ 750</i><span class="subtitle">Plan your investment better and save huge tax . File error free Income Tax Return. Talk to  our tax expert now</span></h3>
							<a href="#" class="button button-border button-border-thin button-palegreen"><i class="icon-phone-sign"></i> 91-86184 14801</a>
						</div>
					</div>

					<div class="clear"></div>
                </div>

                {{-- <div class="container clearfix team">

                    <div class="heading-block left">
                        <h2>Freelance Accounting Professionals</h2>
                        <div class="bar" style="margin: 5px 0;"></div>
                        <span>Quickly Search and contact verified accounting freelancers</span>
                    </div>
                    <!-- ======= Freelancer Section ======= -->

                    <div class="row">

                        <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-autoplay="3000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-lg="3" data-items-xl="4">
                            @if($Users)
                            @foreach($Users as $User)
                            <div class="oc-item">
                                <a href="http://freelancer.accountswale.in/" target="_blank">
                                    @if($User['profile_pic'])
                                        <img src="http://freelancer.taprecruiter.com/Admin/freelancer/profile_pic/{{$User['profile_pic']}}" alt="Image 1" class="cropped" title="Click to view">
                                    @else
                                        <img src="{{URL::asset('UI/images/trainers/male.jpg')}}" alt="Image 1" class="cropped" title="Click to view">
                                    @endif
                                </a>
                                <div class="freelance freelance-content">
                                    <h4>{{$User['first_name']}} {{$User['last_name']}} - <i>{{$User['qualification']}}</i></h4>
                                    <div>Experience - {{$User['experience_years']}} years {{$User['experience_months']}} months</div>
                                    <div>Qualification - {{$User['qualification']}}</div>

                                </div>
                            </div>
                            @endforeach
                            @else

                            @endif
                        </div>

                        <div class="container clearfix mt-1">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 open_frm_bx loaded" data-background-image-url="{{URL::asset('UI/images/openform_rt.jpg')}}" style="background-image: url({{URL::asset('UI/images/openform_rt.jpg')}});
background-repeat: no-repeat;
background-position: right bottom;
border: 1px solid #efefef;
background-color: #fff;padding:20px 0 0 20px;box-shadow: 0 0 8px 0 rgba(0,0,0,0.2)">
                                <h3>Find freelance Accounting Professional near you</h3>
                                <!--<span>Structure your learning and get a certificate to prove it.</span>-->
                                <form action="#" method="post" class="landing-wide-form clearfix mt-0">
                                    <div class="col_four_fifth nobottommargin">
                                        <div class="col_one_third nobottommargin mr-2">
                                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect1" value="">
										<option>Country</option>
										<option>India</option>
										<option>America</option>
										<option>London</option>
										<option>Australia</option>
									</select>
                                        </div>
                                        <div class="col_one_third nobottommargin mr-2">
                                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect2" value="">
										<option>City</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
                                        </div>
                                        <div class="col_one_third nobottommargin mr-2">
                                            <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect3" value="">
										<option>State</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
                                        </div>

                                    </div>
                                    <div class="col_one_fifth col_last nobottommargin">
                                        <!--<button class="btn btn-lg btn-warning btn-block nomargin" value="submit" type="submit">Search</button>-->
                                        <a href="#" class="button button-3d button-rounded button-orange mt-0"><i class="icon-ok"></i>Search</a>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <!-- End Freelancer Section -->

                </div> --}}

                <!--<div class="section topmargin-lg">
					<div class="container clearfix">
<div class="postcontent nobottommargin clearfix">
						<div class="heading-block left">
							<h2>Online Training And Certification Courses</h2>
							<div class="element"></div>
							<div class="bar" style="margin: 5px 0;"></div>
							<span>Empower yourself with the world’s leading experts.</span>
						</div>
	<div  class="training">
	<div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">


						<div class="oc-item">
							<div class="ipost clearfix">

            <div class="testimonial-item">
              <img src="images/training/training1.jpg" class="testimonial-img" alt="">
				<div class="training_content">
              <h2>4 Months Certification Course for B.Com freshers</h2>
              <h3>By Accountswale</h3>

              <p><strong>Training Duration -</strong> 4 Months, 100% Live Online Training</p>

					<p><strong>Key takeaways -</strong> This course covers training on accounting softwares -Tally and Quickbooks, PF/ESI/Labour Act, Basics of Payroll, GST filing and how to perform Accounts Executive Role effectively in the organisation</p>

              <p><strong>Training -</strong> Friday, Saturday and Sunday<br>
				  <strong>Batch Time -</strong> 10 AM to 12 PM OR 6 PM to 8 PM</p>

					<p><strong>Training Fee -</strong> Rs. 8000</p>

				<div class="clear"></div>


				<div class="social">
					<a href=""><i class="icon-facebook facebook"></i></a>
					<a href=""><i class="icon-twitter twitter"></i></a>
					<a href=""><i class="icon-linkedin-in linkedin"></i></a>
				</div>
					<div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
				</div>
            </div>
								</div>
							</div>
				<div class="oc-item">
							<div class="ipost clearfix">

            <div class="testimonial-item">
              <img src="images/training/training2.jpg" class="testimonial-img" alt="">
				<div class="training_content">
              <h2>GSTR 1 , GSTR 3B and GSTR 9 Filing</h2>
              <h3>By CA Nihalchand Jain</h3>

              <p><strong>Training Duration -</strong> 60 Minutes</p>

					<p><strong>Key takeaways -</strong> GST Returns filing - GSTR 1 and GSTR 3B, , Q&A and Participation certificate</p>

              <p><strong>Training -</strong> Sunday<br>
				  <strong>Batch Time -</strong> 3:30 to 5:00 PM</p>

					<p><strong>Training Fee -</strong> Rs. 199</p>

				<div class="clear"></div>


				<div class="social">
					<a href=""><i class="icon-facebook facebook"></i></a>
					<a href=""><i class="icon-twitter twitter"></i></a>
					<a href=""><i class="icon-linkedin-in linkedin"></i></a>
				</div>
					<div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
				</div>
            </div>
								</div>
							</div>
				<div class="oc-item">
							<div class="ipost clearfix">

            <div class="testimonial-item">
              <img src="images/training/training3.png" class="testimonial-img" alt="">
				<div class="training_content">
              <h2>PF/ESI/Labour Act Certification Training</h2>
              <h3>By Accountswale</h3>

              <p><strong>Training Duration -</strong> 4 Hours</p>

					<p><strong>Key takeaways:</strong> PF/ESI/Labour Act rules , registration process,applicability and Returns filing . General question and answers , Participation certificate</p>

              <p><strong>Training -</strong> Sunday<br>
				  <strong>Batch Time -</strong> 10 AM to 2 PM</p>

					<p><strong>Training Fee -</strong> Rs. 899</p>

				<div class="clear"></div>


				<div class="social">
					<a href=""><i class="icon-facebook facebook"></i></a>
					<a href=""><i class="icon-twitter twitter"></i></a>
					<a href=""><i class="icon-linkedin-in linkedin"></i></a>
				</div>
					<div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
				</div>
            </div>
								</div>
							</div>
				<div class="oc-item">
							<div class="ipost clearfix">

            <div class="testimonial-item">
              <img src="images/training/training4.jpg" class="testimonial-img" alt="">
				<div class="training_content">
              <h2>3 month weekend GST Detailed Certification Course</h2>
              <h3>By CA Nihalchand Jain</h3>

              <p><strong>Training Duration -</strong> 3 Months (Duration -51 Hours )</p>

					<p><strong>Key takeaways:</strong> This course will cover: basics of GST that gives the attendee a basic idea about the following topics: a. Meaning of supply b. Time of supply c. Value of supply d. Place of supply e. Input Tax Credit f. Registration. 100% Placement assistance. </p>

              <p><strong>Training -</strong> Saturday and Sunday<br>
				  <strong>Batch Time -</strong> 3:30 PM to 5:00 PM</p>

					<p><strong>Training Fee -</strong> Rs. 4999</p>

				<div class="clear"></div>


				<div class="social">
					<a href=""><i class="icon-facebook facebook"></i></a>
					<a href=""><i class="icon-twitter twitter"></i></a>
					<a href=""><i class="icon-linkedin-in linkedin"></i></a>
				</div>
					<div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
				</div>
            </div>
								</div>
							</div>

<div class="oc-item">
							<div class="ipost clearfix">

            <div class="testimonial-item">
              <img src="images/training/training5.jpg" class="testimonial-img" alt="">
				<div class="training_content">
              <h2>1 Week Tally Live Coaching</h2>
              <h3>By Accountswale</h3>

              <p><strong>Training Duration -</strong> 1 Week</p>

					<p><strong>Key takeaways:</strong>  Learn Basics of Tally . This course is for beginners who wants to learn Tally to manage Accounting job. By attending this course you can expect learning how to maintain accounting using tally software.</p>

              <p><strong>Training -</strong> Monday to Saturday<br>
				  <strong>Batch Time -</strong> 9:30 AM to 11:30 AM</p>

					<p><strong>Training Fee -</strong> Rs. 499</p>

				<div class="clear"></div>


				<div class="social">
					<a href=""><i class="icon-facebook facebook"></i></a>
					<a href=""><i class="icon-twitter twitter"></i></a>
					<a href=""><i class="icon-linkedin-in linkedin"></i></a>
				</div>
					<div class="center"><a href="#" class="button button-3d button-large button-rounded button-aqua">Enroll Now</a></div>
				</div>
            </div>
								</div>
							</div>




					</div>
	</div>
						</div>
						<div class="sidebar nobottommargin col_last clearfix">
							<img src="images/ad_banner.png" alt=""/>
						</div>
						<div class="clear bottommargin-sm"></div>


					</div>
				</div>-->

                {{-- <div class="container clearfix">
                    <a href="#"><img src="{{URL::asset('UI/images/ad_banner.jpg')}}" alt=""></a>
                    <a href="#"><img src="{{URL::asset('UI/images/ad2_banner.jpg')}}" class="mt-4" alt=""></a>
                    <div class="d-flex row mt-4">
                        <div class="col-md-4">
                            <a href="#"><img src="{{URL::asset('UI/images/ad_1.jpg')}}" alt=""></a>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="{{URL::asset('UI/images/ad_3.jpg')}}" alt=""></a>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="{{URL::asset('UI/images/ad_2.jpg')}}" alt=""></a>
                        </div>

                    </div>
                </div> --}}

                <!--<section class="firms">
					<div class="container clearfix">

						<div class="col_one_third nobottommargin">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="background:#fff;">
							<h3>Starting &amp; Managing Your Business has never been easier!<span class="subtitle">Start a Company, Income Tax registration &amp; Filings</span></h3>
							<a href="#" class="button button-border button-border-thin button-palegreen">Enroll now</a>
						</div>
					</div>

					<div class="col_one_third nobottommargin feature_call">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
							<h3>Automated All-in-One Business Solution<span class="subtitle">*Terms And Conditions apply</span></h3>
							<a href="#" class="button button-3d button-large button-rounded button-orange">Call Now: 91-86184 14801</a>
						</div>
					</div>



					<div class="col_one_third nobottommargin col_last">
						<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="background:#fff;">
							<h3>Expert Assisted Tax Filing <i>@ 799</i><span class="subtitle">A proven step-by-step framework to help you launch the next big thing</span></h3>
							<a href="#" class="button button-border button-border-thin button-palegreen">Get Started</a>
						</div>
					</div>

					<div class="clear"></div>
					</div>
				</section>
				<section class="video_trainer">
				<div class="container clearfix">
<div class="col_three_fifth nobottommargin">
						<div class="heading-block">
							<h2>Why Accountswale.in</h2>
						</div>
	<h3>Choosing us to make a difference</h3>
						<p>Income Tax Return ( ITR ) filing checklist for Salary Income /Individual in India.</p>


					</div>

					<div class="col_two_fifth nobottommargin col_last">


						<a href="https://vimeo.com/101373765" data-lightbox="iframe">
							<img src="images/videos/1.jpg" alt="Image">
							<span class="i-overlay"><img src="images/icons/play.png" alt="Play"></span>
						</a>

					</div>

					<div class="clear"></div>

				</div>
				</section>


				<div class="container clearfix team">

					<div class="heading-block center">
							<h2>Meet Our Mentors</h2>
							<div class="bar"></div>
							<span>How we serve you the best</span>
						</div>



        <div class="row">

          <div class="col-lg-6">
            <div class="member d-flex align-items-start">
				<div class="timedate d-flex align-items-start">
					<div>27 July 2020</div>
					<div>Monday</div>
					<i>8:30PM</i>
				</div>
              <div class="pic"><img src="images/trainers/trainer-1.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Suresh Vankar</h4>
                <span>Accountant</span>
                <p>This webinar will help you to create a Powerful Perspective</p>
				  <h5>Strategic Cost Management</h5>
                <div class="social">
                  <a href=""><i class="icon-twitter"></i></a>
                  <a href=""><i class="icon-facebook"></i></a>
                  <a href=""><i class="icon-instagram"></i></a>
                  <a href=""><i class="icon-linkedin-in"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="member d-flex align-items-start">
				<div class="timedate d-flex align-items-start">
					<div>28 July 2020</div>
					<div>Tuesday</div>
					<i>8:30PM</i>
				</div>
              <div class="pic"><img src="images/trainers/trainer-2.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Rajesh Sinha</h4>
                <span>GSTR Filing</span>
                <p>This webinar will help you to create a Powerful Perspective</p>
				  <h5>Circumvent unwarranted expenses</h5>
                <div class="social">
                  <a href=""><i class="icon-twitter"></i></a>
                  <a href=""><i class="icon-facebook"></i></a>
                  <a href=""><i class="icon-instagram"></i></a>
                  <a href=""><i class="icon-linkedin-in"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start">
				<div class="timedate d-flex align-items-start">
					<div>29 July 2020</div>
					<div>Wednesday</div>
					<i>8:30PM</i>
				</div>
              <div class="pic"><img src="images/trainers/trainer-3.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Sarju Kaira</h4>
                <span>Yoga Courses</span>
                <p>This webinar will help you to create a Powerful Perspective</p>
				  <h5>Lower financial expenditures</h5>
                <div class="social">
                  <a href=""><i class="icon-twitter"></i></a>
                  <a href=""><i class="icon-facebook"></i></a>
                  <a href=""><i class="icon-instagram"></i></a>
                  <a href=""><i class="icon-linkedin-in"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start">
				<div class="timedate d-flex align-items-start">
					<div>30 July 2020</div>
					<div>Thursday</div>
					<i>8:30PM</i>
				</div>
              <div class="pic"><img src="images/trainers/trainer-4.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Nishanth</h4>
                <span>Business Courses</span>
                <p>This webinar will help you to create a Powerful Perspective</p>
				  <h5>Space Management</h5>
                <div class="social">
                  <a href=""><i class="icon-twitter"></i></a>
                  <a href=""><i class="icon-facebook"></i></a>
                  <a href=""><i class="icon-instagram"></i></a>
                  <a href=""><i class="icon-linkedin-in"></i> </a>
                </div>
              </div>
            </div>
          </div>

        </div>




				</div>-->
                {{-- <div class="section topmargin-lg">
                    <div class="container clearfix">

                        <div class="heading-block center">
                            <h2>Apply Jobs</h2>
                            <div class="bar"></div>
                            <span>We help you for Job Placement</span>
                        </div>
                        <div class="row">
                            <div id="oc-images1" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-autoplay="3000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-lg="3" data-items-xl="4">

                                @if($FreeJobs)
                                @foreach($FreeJobs as $Jobs)
                                    <div class="oc-item">

                                        <div class="freelance freelance-content">
                                            <h5>{{$Jobs->employer_name}} </h5>
                                            <h4>{{$Jobs->job_title}}</h4>
                                            <div><i>{{$Jobs->qualification}}</i></div>
                                            <div>{{$Jobs->location}}</div>
                                            <div class="left mt-3"><a href="#apply-jobs" data-lightbox="inline" class="button button-3d button-mini button-rounded button-aqua button-light text-white" id="ApplyJobs" data-id="{{$Jobs->id}}">Apply</a></div>
                                        </div>

                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h2>Useful Youtube Videos</h2>
                        <div class="bar"></div>
                        <!--<span>Learn from our Videos</span>-->
                    </div>
                    <div id="portfolio" class="portfolio grid-container clearfix">
                        @if($Videos)
                        @foreach($Videos as $Video)
                        <article class="portfolio-item pf-graphics pf-uielements">
                            <div class="portfolio-image">
                                {{-- <a href="#">
                                    <img src="{{URL::asset('UI/images/videos/2.jpg')}}" alt="Accountswale">
                                </a> --}}
                                <div class="portfolio-overlay">
                                    <a href="{{$Video->url}}" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>

                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="{{$Video->url}}" data-lightbox="iframe">{{$Video->video_title}}</a></h3>
                            </div>
                        </article>
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="section topmargin-lg">
                    <div class="container clearfix">
                        <div class="heading-block center">
                            <h2>Knowledge Portal</h2>
                            <div class="bar"></div>
                            <span>Blogs and News Articles</span>
                        </div>
                        <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
                            @if($Questions)
                            @foreach($Questions as $Question)
                            <div class="entry clearfix">
                                <div class="entry-image">
                                    <a href="/blogs/details/{{$Question['id']}}/{{$Question['slug']}}" target="_blank" data-lightbox="image"><img class="image_fade" src="https://onlinelms.skillsgroom.com/UI/questions/{{$Question['feature_img']}}" alt="Standard Post with Image" style="height: 200px;"></a>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><a href="#"><i class="icon-male1"></i> {{$Question['tags']}}</a></li>
                                    <li><i class="icon-calendar3"></i>{{date('M d Y', strtotime($Question['created_at']))}} at {{date('h:i a', strtotime($Question['created_at']))}}</li>
                                </ul>

                                <div class="entry-content">
                                    <div class="entry-title">
                                        <h2><a href="/blogs/details/{{$Question['id']}}/{{$Question['slug']}}" target="_blank">{{$Question['topic']}}</a></h2>
                                    </div>
                                    <a href="/blogs/details/{{$Question['id']}}/{{$Question['slug']}}" target="_blank" class="more-link">Read More</a>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <!-- #content end -->

        <div class="modal1 mfp-hide" id="apply-jobs">
			<div class="card divcenter" style="max-width: 540px;">
				<div class="card-header py-3 nobg center">
					<h3 class="mb-0 t400">Welcome to Apply Jobs</h3>
				</div>
				<div class="card-body divcenter py-5" style="max-width: 95%;">
<form id="apply-form" name="apply-form" class="nobottommargin row" action="/apply_jobs" method="post" enctype="multipart/form-data">
    @csrf
						<div class="col-6">
                            <input type="hidden" id="job_id" name="job_id" value="" class="form-control not-dark" placeholder="id" />

							<input type="text" id="login-form-firstname" name="first_name" value="" class="form-control not-dark" placeholder="First name" />
						</div>
	<div class="col-6">
							<input type="text" id="login-form-lastname" name="last_name" value="" class="form-control not-dark" placeholder="Last name" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-email" name="email" value="" class="form-control not-dark" placeholder="Email Id" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-contact" name="contact" value="" class="form-control not-dark" placeholder="Contact Number" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-country" name="country" value="" class="form-control not-dark" placeholder="Country" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-state" name="state" value="" class="form-control not-dark" placeholder="State" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-city" name="city" value="" class="form-control not-dark" placeholder="City" />
						</div>
	<div class="col-6 mt-4">
							<input type="text" id="login-form-area" name="area" value="" class="form-control not-dark" placeholder="Area" />
						</div>
		<div class="col-6 mt-4">
							<input type="text" id="login-form-pincode" name="pincode" value="" class="form-control not-dark" placeholder="Pincode" />
						</div>
	<div class="col-6 mt-4">
							<input type="file" id="login-form-resume" name="resume" value="" class="form-control not-dark" placeholder="Upload Resume" />
						</div>



						<div class="col-12 mt-4">
							<button class="button button-rounded btn-block nomargin" id="login-form-apply" name="login-form-apply"  type="submit" value="Apply Jobs">Apply Jobs</button>
						</div>
					</form>
				</div>

			</div>
		</div>
@endsection


@section('JSScript')
    <script>
        $(document).on("click", '#ApplyJobs', function(){
            var job_id = $(this).data('id');
            $("#job_id").val(job_id);
        });

        $(document).on("click", '#login-form-apply', function(){
            alert("Thanks For Applying Job...")
        });
    </script>
@endsection
