@extends('UI.base')
@section('Content')
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix pt-0">

            <div class="col_one_third nobottommargin">

                
    <div class="login_left">
        <div class="login_left_img"><img src="{{URL::asset('UI/images/post_job.jpg')}}" alt="login background" ></div>
    </div>


            </div>

            <div class="col_two_third col_last nobottommargin">

<div class="heading-block left">
                    <h4>Post a Job</h4>
                    <div class="bar" style="margin: 5px 0;"></div>
                    
                </div>
                

<!--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, vel odio non dicta provident sint ex autem mollitia dolorem illum repellat ipsum aliquid illo similique sapiente fugiat minus ratione.</p>-->
                    @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                    @endif
                <form id="register-form" name="register-form" class="nobottommargin" action="/AddFreeJobs" method="post">
                    @csrf
                    <div class="col_half">
                        <label for="register-form-fname">Employer Name:</label>
                        <input type="text" id="register-form-name" name="employer_name" value="" class="form-control" required/>
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-email">Email Address:</label>
                        <input type="text" id="register-form-email" name="email" value="" class="form-control" required/>
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-number">Contact Number:</label>
                        <input type="text" id="register-form-number" name="contact" value="" class="form-control" required/>
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-title">Job Title:</label>
                        <input type="text" id="register-form-title" name="job_title" value="" class="form-control" required/>	
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-location">Job Location:</label>
                        <input type="text" id="register-form-location" name="location" value="" class="form-control" required/>
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-job">Is this working from Home Job ?</label>
                        
                        <select id="register-form-job" name="work_from_home" value="" class="form-control" required>
                        <option>Yes</option>
                            <option>No</option>
                        </select>
                    </div>

                    <div class="clear"></div>
                        <div class="col_half">
                            <label for="register-form-qualification">Required Qualifications :</label>
                        <input type="text" id="register-form-qualification" name="qualification" value="" class="form-control" required/>
                    </div>
<div class="col_half col_last">
                        <label for="register-form-exp">Required Experience :</label>
                        <input type="text" id="register-form-exp" name="experience" value="" class="form-control" required/>
                    </div>
                    <div class="clear"></div>
<div class="col_half">
                            <label for="register-form-salary">Salary Budget:</label>
                        <input type="text" id="register-form-salary" name="salary_budget" value="" class="form-control" required/>
                    </div>
<div class="col_half col_last">
                        <label for="register-form-desc">Job description :</label>
                        
                        <textarea class="form-control" name="job_description" id="" cols="30" rows="10" required></textarea>
                    </div>
                    <div class="clear"></div>
                    <div class="col_full nobottommargin">
                        <button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit">Post A Job</button>
                    </div>
                </form>

            </div>

        </div>

    </div>

</section>
@endsection