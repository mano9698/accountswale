@extends('UI.base')
@section('Content')
<section id="slider" class="slider-element slider-parallax"  data-height-xl="500" data-height-lg="400" data-height-md="300" data-height-sm="200" data-height-xs="150">
    <div class="fslider" data-arrows="true" data-pagi="false" data-lightbox="gallery">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide"><img class="image_fade" src="{{URL::asset('UI/images/franchise.jpg')}}" alt="Standard Post with Gallery"></div>
                                        </div>
                                    </div>
                                </div>
    </section>

<section id="content">
    <div class="content-wrap">

        <div class="container pt-4 pb-5">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6 mb-5 mb-md-0">
                    <div class="feature-box fbox-plain bottommargin-sm">
                        <div class="fbox-icon">
                            <i class="icon-line-circle-check text-primary"></i>
                        </div>
                        <div class="fbox-content">
                            <h3 class="font-weight-normal nott">Interested in Starting your own business or Accounting Firm?</h3>
                            <p>Become a Franchisee with one of the fastest growing Accounting Freelance Company In India.</p>
                        </div>
                    </div>

                    <div class="feature-box fbox-plain bottommargin-sm">
                        <div class="fbox-icon">
                            <i class="icon-line-circle-check text-warning"></i>
                        </div>
                        <div class="fbox-content">
                            <h3 class="font-weight-normal nott">Why to become a Franchisee partner with Accountswale?</h3>
                            <ul class="iconlist my-3" style="font-size: 15px; line-height: 22px; color: #999;">
                            <li><i class="icon-star3"></i>20+ years of Accounting Industry Presence </li>
                                <li><i class="icon-star3"></i>Opportunities to get inquiries across India and overseas </li>
                                <li><i class="icon-star3"></i>Grow your income in multifolds in next 2 years </li>
                                <li><i class="icon-star3"></i>24X7 on call support services </li>
                                <li><i class="icon-star3"></i>Associated Network of 500+ CA Firms for any Audit /Compliance Assistance </li>
                                <li><i class="icon-star3"></i>Dedicated Dashboard to Track all your inquiries </li>
                            </ul>
                        </div>
                    </div>


                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon">
                            <i class="icon-line-circle-check text-danger"></i>
                        </div>
                        <div class="fbox-content">
                            <h3 class="font-weight-normal nott">Investment and space required</h3>
                            <ul class="iconlist my-3" style="font-size: 15px; line-height: 22px; color: #999;">
                                <li><i class="icon-star3"></i>Capital Investment - INR 75,000 to INR 1,00,000 </li>
                                <li><i class="icon-star3"></i>Space required - 100 -150 Sqft</li>
                                <li><i class="icon-star3"></i>Best processes and training support </li>
                                <li><i class="icon-star3"></i>Guaranteed attractive revenue on Franchise investment</li>
                                <li><i class="icon-star3"></i>Minimum Yearly income of 5 Lakhs for the 1st year</li>
                                <li><i class="icon-star3"></i>30% to 50% growth projection year on year</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-6">
                    <div class="card pricing border-0 shadow bg-color dark">
                        <div class="card-body rounded pb-0 px-4 px-lg-5 pt-4 pt-lg-5">
                            {{-- <div class="d-block d-lg-flex flex-row justify-content-lg-between align-items-lg-center">
                                <h3 class="h6 font-weight-bolder mb-2 mb-lg-0 text-white">Want to Join? Why Wait</h3>
                                 <a href="#" class="button button-small button-light button-white button-rounded m-0">Pay Now</a>
                                <h4>+91 86184 14801</h4>
                            </div> --}}
                            <div class="line line-sm"></div>
                            <h3 class="h5 font-weight-bolder mb-3 text-white">Sign Up</h3>
                            <p class="text-smaller mb-0" style="line-height: 1.5;">Fill the form to register</p>
                            @if(session('message'))
                    <div class="alert alert-success width100">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                    @endif
                    <form id="register-form" name="register-form" class="nobottommargin" action="/AddFranchisee" method="post">
                        @csrf
                    <div class="col_half mt-3">
                        <label for="register-form-fname">First Name:</label>
                        <input type="text" name="first_name" id="register-form-fname" name="register-form-fname" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last mt-3">
                        <label for="register-form-lname">Last Name:</label>
                        <input type="text" name="last_name" id="register-form-lname" name="register-form-lname" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-email">Email ID:</label>
                        <input type="text" id="register-form-email"  name="email" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-phone">Contact Number:</label>
                        <input type="text" id="register-form-phone"  name="mobile" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>

                    <div class="col_half">
                        <label for="register-form-country">Country:</label>
                        <input type="text" id="register-form-country"  name="country" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-state">State:</label>
                        <input type="text" id="register-form-state"  name="state" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>
                        <div class="col_half">
                            <label for="register-form-city">City:</label>
                        <input type="text" id="register-form-city"  name="city" value="" class="form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="register-form-pincode">Pincode:</label>
                        <input type="text" id="register-form-pincode"  name="pincode" value="" class="form-control" />
                    </div>

                    <div class="clear"></div>
                        <div class="col_half">

                        <label for="register-form-area">Area:</label>
                        <input type="text" id="register-form-area"  value="" name="area" class="form-control" />
                    </div>

                    <div class="clear"></div>

                    <div class="col-12 text-center">
                                    <button class="btn btn rounded bg-dark text-white text-uppercase font-weight-semibold ls1 py-2 px-5" id="signup-form-submit1" name="signup-form-submit1" value="login" type="submit">Sign Up</button>
                                </div>
                    </form>

                        </div>
                    </div>
                </div>
            </div>
            </div>

    </div>

</section>

@endsection
