<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <meta name="google-site-verification" content="LQv-hhKKice8g_VQKuU6fctFIIIfup8QcSnG-wSNvzk" />

    <meta name="description" content="Accountswale">
  <meta name="keywords" content="GST registration, GST Return Filing, Personal ITR Filing, Income Tax Filing, New Company Registration, Best Auditor in India, Best CA Firm In India, Accounting Freelance, Top Accounting Firms in Bangalore, Virtual Office in India, Virtual Office in Bangalore, Investment Declaration, Remote Accounting Services, PF Consultant in Bangalore, ESI Consultant in Bangalore, Best GST Training, Payroll consultant, Best payroll staffing, How to find best CA Firm
  ">

    <!-- Stylesheets
	============================================= -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:200,300,400,500,600,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/main_style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/swiper.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
	============================================= -->
    <title>{{$title}}</title>

</head>

<body class="stretched">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <div class="modal1 mfp-hide" id="modal-register">
            <div class="card divcenter" style="max-width: 540px;">
                <div class="card-header py-3 nobg center">
                    <h3 class="mb-0 t400">Hello, Welcome Back</h3>
                </div>
                <div class="card-body divcenter py-5" style="max-width: 70%;">
                    <form id="login-form" name="login-form" class="nobottommargin row" action="#" method="post">

                        <div class="col-12">
                            <input type="text" id="login-form-username" name="login-form-username" value="" class="form-control not-dark" placeholder="Username" />
                        </div>

                        <div class="col-12 mt-4">
                            <input type="password" id="login-form-password" name="login-form-password" value="" class="form-control not-dark" placeholder="Password" />
                        </div>

                        <div class="col-12">
                            <a href="#" class="fright text-dark t300 mt-2">Forgot Password?</a>
                        </div>

                        <div class="col-12 mt-4">
                            <button class="button button-rounded btn-block nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                        </div>
                    </form>
                </div>
                <div class="card-footer py-4 center">
                    <p class="mb-0">Don't have an account? <a href="register.html"><u>Sign up</u></a></p>
                </div>
            </div>
        </div>

        <!-- Top Bar
		============================================= -->
        @include('UI.common.top_bar')
        <!-- #top-bar end -->

        <!-- Header
        ============================================= -->
        @include('UI.common.header')

        <!-- #header end -->

        <!-- Content
		============================================= -->
        @yield('Content')
        <!-- #content end -->

        <!-- Footer
		============================================= -->
        @include('UI.common.footer')
        <!-- #footer end -->

    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
	============================================= -->
    <script src="{{URL::asset('UI/js/jquery.js')}}"></script>
    <script src="{{URL::asset('UI/js/plugins.js')}}"></script>

    <!-- Footer Scripts
	============================================= -->
    <script src="{{URL::asset('UI/js/functions.js')}}"></script>
    <script src="{{URL::asset('UI/js/youtube.js')}}"></script>
    <script src="{{URL::asset('UI/js/custom.js')}}"></script>
    <script src="//code.tidio.co/nowbvenvldrajiadrdok4peivqwrdott.js" async></script>
    @yield('JSScript')
</body>

</html>
