<footer id="footer" class="dark">

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">

            <div class="col_two_third">

                <div class="col_one_third">

                    <div class="widget clearfix">
                        <h4>Follow Us</h4>
                        <a href="https://www.facebook.com/accountswale.in" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="https://www.instagram.com/accountswale/" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin" style="margin-right: 10px;">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/accountswale/" target="_blank" class="social-icon si-dark si-colored si-linkedin nobottommargin" style="margin-right: 10px;">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>

                </div>
                <div class="col_one_third">
                    <div class="widget widget_links clearfix">

                        <h4>Useful website links</h4>
                        <ul>
                            <li><a href="http://www.mca.gov.in/">MCA Official website</a></li>
                            <li><a href="https://www.gst.gov.in/">GST official website</a></li>
                            <li><a href="https://www.incometaxindia.gov.in/Pages/default.aspx">Income Tax official website</a></li>
                            <li><a href="https://www.incometaxindiaefiling.gov.in/home">Income Tax Return Filing Website</a></li>
                            <li><a href="https://www.epfindia.gov.in/site_en/index.php">EPFO official website</a></li>
                            <li><a href="https://www.esic.nic.in/">ESIC official website</a></li>
                            <li><a href="https://www.icai.org/">ICAI Official website</a></li>
                            <li><a href="http://www.ipindia.nic.in/index.htm">Trademark official website</a></li>
                            <li><a href="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp">TDS online payment</a></li>
                            <li><a href="https://www.onlineservices.nsdl.com/paam/endUserRegisterContact.html">Online PAN Application</a></li>
                            <li><a href="https://unifiedportal-mem.epfindia.gov.in/memberinterface/">PF online member login portal</a></li>
                            <li><a href="https://unifiedportal-emp.epfindia.gov.in/epfo/">PF Online payment portal for employer</a></li>

                        </ul>

                    </div>
                </div>
                <div class="col_one_third col_last">
                    <div class="widget widget_links clearfix">

                        <h4>Informations</h4>

                        <ul>
                            <!--<li><a href="about.html">About Us</a></li>
                            <li><a href="return.html">Return</a></li>-->
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms Of Use</a></li>
                        </ul>

                    </div>
                </div>

            </div>

            <div class="col_one_third col_last">

                <div class="widget clearfix">

                    <h4>Locations</h4>

                    <address>
                                3rd Floor, Kiran Tower , Service Road, <br>
                                Teacher's Colony, Sector 5, HSR Layout, Near Silk Board Junction,<br>
                                Bengaluru, Karnataka 560034. 
                            </address>
                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 86184 14801<br>
                    <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:services@accountswale.in">services@accountswale.in</a>

                </div>

            </div>

        </div>
        <!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            © 2020 <a href="https://www.techitalents.com/">Techtalents Software Technologies Pvt Ltd</a>. All rights reserved.

        </div>

    </div>
    <!-- #copyrights end -->

</footer>