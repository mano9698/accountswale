<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItrTax extends Model
{
    use HasFactory;

    protected $table = 'income_tax_declaration';

    protected $fillable = ['user_id','year', 'payslip', 'form16', 'investment', 'amount', 'proof', 'itr', 'type','file_type'];
}
