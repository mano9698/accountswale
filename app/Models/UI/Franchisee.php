<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Franchisee extends Model
{
    use HasFactory;

    protected $table = 'franchisee';

    protected $fillable = ['first_name', 'last_name', 'email', 'mobile', 'country', 'state', 'city', 'pincode', 'area'];
}
