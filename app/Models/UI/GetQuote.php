<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GetQuote extends Model
{
    use HasFactory;

    protected $table = 'get_quote';

    protected $fillable = ['user_id','name', 'email', 'contact', 'country', 'state', 'city', 'service', 'your_client', 'requirement', 'budget', 'questions_requirement'];
}
