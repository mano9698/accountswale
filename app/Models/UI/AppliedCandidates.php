<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppliedCandidates extends Model
{
    use HasFactory;

    protected $table = 'applied_candidates';

    protected $fillable = ['job_id', 'first_name', 'last_name', 'contact', 'email', 'country', 'state', 'city', 'area', 'pincode', 'resume'];
}
