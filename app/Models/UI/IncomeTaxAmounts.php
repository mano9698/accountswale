<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncomeTaxAmounts extends Model
{
    use HasFactory;

    protected $table = 'income_tax_amounts';

    protected $fillable = ['income_tax_id','investment', 'amount'];
}
