<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Model;

class FreeJobs extends Model
{
    protected $table = 'free_jobs';

    protected $fillable = ['jobs_id', 'employer_name', 'job_title', 'slug', 'contact', 'email', 'location', 'work_from_home', 'qualification', 'experience', 'salary_budget', 'job_description'];

}
