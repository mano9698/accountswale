<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthendicationController extends Controller
{
    public function login(){
        $title = "Admin | Login";

        return view('Admin.login', compact('title'));
    }

    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');

            }elseif($CheckEmail->user_type == 1){
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    $request->session()->put('AdminProfilePic', Auth::guard('super_admin')->user()->profile_pic);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                    // echo "Success";
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }elseif($CheckEmail->user_type == 2){
                if (Auth::guard('user')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('UserFname', Auth::guard('user')->user()->first_name);
                    $request->session()->put('UserLname', Auth::guard('user')->user()->last_name);
                    $request->session()->put('UserEmail', Auth::guard('user')->user()->email);
                    $request->session()->put('UserId', Auth::guard('user')->user()->id);
                    $request->session()->put('UserProfilePic', Auth::guard('user')->user()->profile_pic);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('users/dashboard');
                    // echo "Success";
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }
        }

    }
}
