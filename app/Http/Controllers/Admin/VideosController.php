<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Videos;

class VideosController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        $this->middleware('guest:super_admin');
    }

    public function add_videos(){
        $title ="Add Videos";

        return view('Admin.videos.add_videos', compact('title'));
    }

    public function edit_videos($id){
        $title ="Edit Videos";
        $Videos = Videos::where('id', $id)->first();
        return view('Admin.videos.edit_videos', compact('title', 'Videos'));
    }

    public function store_videos(Request $request){
        $Videos = new Videos();

        $Videos->video_title = $request->video_title;
        $Videos->url = $request->url;
        $Videos->status = 0;

        $Videos->save();

        return redirect()->back()->with('message','Video Posted Successfully');

    }

    public function update_videos(Request $request){
        $id = $request->id;
        $Videos = Videos::where('id', $id)->first();

        $Videos->video_title = $request->video_title;
        $Videos->url = $request->url;

        $Videos->save();

        return redirect()->back()->with('message','Video Updated Successfully');

    }

    public function delete_videos($id){

        $Videos = Videos::where('id', $id)->delete();

        return redirect()->back()->with('message','Video Deleted Successfully');

    }

    public function changeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $Videos = Videos::find($request->id);
        $Videos->status = $request->status;
        $Videos->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    public function videos_list(){
        $title ="Videos List";
        $Videos = Videos::get();
        return view('Admin.videos.videos_list', compact('title', 'Videos'));
    }
}
