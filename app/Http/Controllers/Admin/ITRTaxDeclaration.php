<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;
use App\Models\UI\FreeJobs;
use App\Models\UI\ItrTax;
use App\Models\UI\IncomeTaxAmounts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class ITRTaxDeclaration extends Controller
{
    public function income_tax_list(){
        $title ="Income Tax Declaration List";
        if(Auth::guard('super_admin')->check()){

            $ItrTax = ItrTax::get();
        }else{
            $UserId = Session::get('UserId');
            $ItrTax = ItrTax::where('type', 1)->where('user_id', $UserId)->get();
        }

        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.income_tax.income_tax_list', compact('title', 'ItrTax'));
    }

    public function add_income_tax(){
        $title ="Add Income Tax Declaration";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.income_tax.new_income_tax', compact('title', 'Users'));
    }

    public function edit_income_tax($id){
        $title = "Edit Income Tax Declaration";
        $ItrTax = ItrTax::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        // $Banks = Banks::get();
        return view('Admin.income_tax.edit_income_tax', compact('title', 'ItrTax', 'Users'));
    }

    public function store_income_tax(Request $request){

        $ItrTax = new ItrTax();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }

        $ItrTax->year = $request->year;
        $ItrTax->type = 1;
        $AddCustomers = $ItrTax->save();

        // $request->session()->put('CustomerId', $ItrTax->id);

        $InvestmentArr = $_POST['investment'];
        $AmountArr = $_POST['amount'];

        if(!empty($AmountArr)){

            for($i = 0; $i < count($AmountArr); $i++){
                if(!empty($AmountArr[$i])){

                    $IncomeTaxAmounts = new IncomeTaxAmounts();
                    $IncomeTaxAmounts->income_tax_id = $ItrTax->id;
                    $IncomeTaxAmounts->investment = $InvestmentArr[$i];
                    $IncomeTaxAmounts->amount = $AmountArr[$i];

                    $IncomeTaxAmounts->save();

                    // echo json_encode($AmountArr[$i]);
                    // Database insert query goes here
                }
            }
        }

        // exit;

        return redirect()->back()->with('message','Income tax Declaration Updated Successfully');


    }

    public function update_income_tax(Request $request){

        // if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $ItrTax = ItrTax::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }

        $ItrTax->year = $request->year;

        $AddCustomers = $ItrTax->save();


        $InvestmentArr = $_POST['investment'];
        $AmountArr = $_POST['amount'];

        IncomeTaxAmounts::where('income_tax_id', $ItrTax->id)->delete();

        $InvestmentArr = $_POST['investment'];
        $AmountArr = $_POST['amount'];

        if(!empty($AmountArr)){

            for($i = 0; $i < count($AmountArr); $i++){
                if(!empty($AmountArr[$i])){

                    $IncomeTaxAmounts = new IncomeTaxAmounts();
                    $IncomeTaxAmounts->income_tax_id = $ItrTax->id;
                    $IncomeTaxAmounts->investment = $InvestmentArr[$i];
                    $IncomeTaxAmounts->amount = $AmountArr[$i];

                    $IncomeTaxAmounts->save();

                    // echo json_encode($AmountArr[$i]);
                    // Database insert query goes here
                }
            }
        }

        return redirect()->back()->with('message','Income Tax Declaration Updated Successfully');

    }

    public function delete_income_tax($id)
    {
    	// \Log::info($request->all());
        $IncomeTaxAmounts = IncomeTaxAmounts::where('id', $id)->delete();

        return redirect()->back()->with('message','Income Tax Declaration Deleted Successfully');
    }

    public function delete_income_tax_lists($id)
    {
    	// \Log::info($request->all());
        $ItrTax = ItrTax::where('id', $id)->delete();

        $IncomeTaxAmounts = IncomeTaxAmounts::where('income_tax_id', $id)->delete();

        return redirect()->back()->with('message','Income Tax Declaration Deleted Successfully');
    }



    // Payslip
    public function payslip_list(){
        $title ="Payslip List";
        if(Auth::guard('super_admin')->check()){

            $ItrTax = ItrTax::get();
        }else{
            $UserId = Session::get('UserId');
            $ItrTax = ItrTax::where('type', 2)->where('user_id', $UserId)->get();
        }
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.payslips.payslip_list', compact('title', 'ItrTax'));
    }

    public function new_payslip(){
        $title ="New Payslip List";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.payslips.new_payslip', compact('title', 'Users'));
    }

    public function edit_payslip($id){
        $title ="Edit Payslip List";
        $ItrTax = ItrTax::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.payslips.edit_payslip', compact('title', 'ItrTax', 'Users'));
    }

    public function store_payslip(Request $request){


        $ItrTax = new ItrTax();
        $ItrTax->year = $request->year;

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }


        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/payslip/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }


        $ItrTax->type = 2;
        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Payslip Updated Successfully');


    }


    public function update_payslip(Request $request){

        $id = $request->id;
        $ItrTax = ItrTax::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }

        $ItrTax->year = $request->year;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/payslip/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }else{
                $ItrTax->proof = $ItrTax->proof;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }

        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Payslip Updated Successfully');
    }

    public function delete_payslip($id)
    {
    	// \Log::info($request->all());
        $ItrTax = ItrTax::where('id', $id)->delete();

        return redirect()->back()->with('message','Payslip Deleted Successfully');
    }

    // Form 16
    public function form16_list(){
        $title ="Form16 List";
        if(Auth::guard('super_admin')->check()){

            $ItrTax = ItrTax::get();
        }else{
            $UserId = Session::get('UserId');
            $ItrTax = ItrTax::where('type', 3)->where('user_id', $UserId)->get();
        }
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.form16.form16', compact('title', 'ItrTax'));
    }

    public function new_form16(){
        $title ="New Form16";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.form16.new_form16', compact('title', 'Users'));
    }

    public function edit_form16($id){
        $title ="Edit Payslip List";
        $ItrTax = ItrTax::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.form16.edit_form16', compact('title', 'ItrTax', 'Users'));
    }

    public function store_form16(Request $request){


        $ItrTax = new ItrTax();
        $ItrTax->year = $request->year;

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/form16/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }


        $ItrTax->type = 3;
        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Form16 Updated Successfully');


    }


    public function update_form16(Request $request){


        $id = $request->id;
        $ItrTax = ItrTax::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }
        $ItrTax->year = $request->year;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/form16/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }else{
                $ItrTax->proof = $ItrTax->proof;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }

        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Form16 Updated Successfully');
    }

    public function delete_form16($id)
    {
    	// \Log::info($request->all());
        $ItrTax = ItrTax::where('id', $id)->delete();

        return redirect()->back()->with('message','Form16 Deleted Successfully');
    }


    // Investment
    public function investment_list(){
        $title ="Investment List";
        if(Auth::guard('super_admin')->check()){

            $ItrTax = ItrTax::get();
        }else{
            $UserId = Session::get('UserId');
            $ItrTax = ItrTax::where('type', 4)->where('user_id', $UserId)->get();
        }

        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.investment.investment_list', compact('title', 'ItrTax'));
    }

    public function new_investment(){
        $title ="New Investment";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.investment.new_investment', compact('title', 'Users'));
    }

    public function edit_investment($id){
        $title ="Edit Investment";
        $ItrTax = ItrTax::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.investment.edit_investment', compact('title', 'ItrTax', 'Users'));
    }

    public function store_investment(Request $request){


        $ItrTax = new ItrTax();
        $ItrTax->year = $request->year;

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }
        $ItrTax->investment = $request->investment;
        $ItrTax->amount = $request->amount;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/investment/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }


        $ItrTax->type = 4;
        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Investment Updated Successfully');


    }


    public function update_investment(Request $request){


        $id = $request->id;
        $ItrTax = ItrTax::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }
        $ItrTax->year = $request->year;
        $ItrTax->investment = $request->investment;
        $ItrTax->amount = $request->amount;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/investment/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }else{
                $ItrTax->proof = $ItrTax->proof;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }

        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Investment Updated Successfully');
    }

    public function delete_investment($id)
    {
    	// \Log::info($request->all());
        $ItrTax = ItrTax::where('id', $id)->delete();

        return redirect()->back()->with('message','Investment Deleted Successfully');
    }


    // ITR
    public function itr_list(){
        $title ="Filed  ITR List";
        if(Auth::guard('super_admin')->check()){

            $ItrTax = ItrTax::get();
        }else{
            $UserId = Session::get('UserId');
            $ItrTax = ItrTax::where('type', 5)->where('user_id', $UserId)->get();
        }
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.itr.itr_list', compact('title', 'ItrTax'));
    }

    public function new_itr(){
        $title ="Filed  New ITR";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.itr.new_itr', compact('title', 'Users'));
    }

    public function edit_itr($id){
        $title ="Filed  Edit ITR";
        $ItrTax = ItrTax::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.itr.edit_itr', compact('title', 'ItrTax', 'Users'));
    }

    public function store_itr(Request $request){
        $ItrTax = new ItrTax();
        $ItrTax->year = $request->year;

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }
        $ItrTax->investment = $request->investment;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/itr/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }


        $ItrTax->type = 5;
        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Filed  ITR Updated Successfully');


    }


    public function update_itr(Request $request){
        $id = $request->id;
        $ItrTax = ItrTax::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){

            $ItrTax->user_id = $request->user_id;
        }else{
            $UserId = Session::get('UserId');
            $ItrTax->user_id = $UserId;
        }

        $ItrTax->year = $request->year;
        $ItrTax->investment = $request->investment;
        $ItrTax->file_type = $request->file_type;
        if($request->file_type == 1){
            if($request->hasfile('files')){
                $extension = $request->file('files')->getClientOriginalExtension();
                $dir = 'UI/itr/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('files')->move($dir, $filename);

                $ItrTax->proof = $filename;
            }else{
                $ItrTax->proof = $ItrTax->proof;
            }
        }elseif($request->file_type == 2){
            $ItrTax->proof = $request->links;
        }

        $AddCustomers = $ItrTax->save();


        return redirect()->back()->with('message','Filed  ITR Updated Successfully');
    }

    public function delete_itr($id)
    {
    	// \Log::info($request->all());
        $ItrTax = ItrTax::where('id', $id)->delete();

        return redirect()->back()->with('message','Filed  ITR Deleted Successfully');
    }
}
