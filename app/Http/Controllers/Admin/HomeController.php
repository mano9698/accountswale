<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;
use App\Models\UI\FreeJobs;
use App\Models\UI\AppliedCandidates;

use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\UI\Franchisee;

class HomeController extends Controller
{
    // public function __construct(){
    //     // $this->middleware('auth');
    //     // $this->middleware('guest:super_admin')->except('dashboard');
    //     $this->middleware('guest:super_admin');
    // }

    public function dashboard(){
        $title ="Dashboard";

        return view('Admin.layouts.dashboard', compact('title'));
    }

    public function user_dashboard(){
        $title ="Dashboard";

        return view('Admin.layouts.dashboard', compact('title'));
    }

    public function change_password(){
        $title ="Change Password";
        $UserId = Session::get('UserId');
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.layouts.change_password', compact('title'));
    }

    public function jobs_list(){
        $title ="Jobs List";
        $FreeJobs = FreeJobs::get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.layouts.jobs_list', compact('title', 'FreeJobs'));
    }

    public function candidates_list($id){
        $title ="Candidates List";
        $AppliedCandidates = AppliedCandidates::where('job_id', $id)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.layouts.candidates_list', compact('title', 'AppliedCandidates'));
    }

    public function franchisee_list(){
        $title ="Franchisee List";
        $Franchisee = Franchisee::orderBy('created_at', 'desc')->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.layouts.franchisee_list', compact('title', 'Franchisee'));
    }

    public function delete_jobs($id){

        $FreeJobs = FreeJobs::where('id', $id)->delete();

        return redirect()->back()->with('message','Job Deleted Successfully');

    }

    public function changejobstatus(Request $request)
    {
    	// \Log::info($request->all());
        $FreeJobs = FreeJobs::find($request->id);
        $FreeJobs->status = $request->status;
        $FreeJobs->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    public function update_password(Request $request)
    {

        //  $this->validate($request, [

        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);

        $UserId = Session::get('UserId');

       $hashedPassword = Auth::guard('user')->user()->password;

    //    if (\Hash::check($request->oldpassword , $hashedPassword )) {

         if (!\Hash::check($request->newpassword , $hashedPassword)) {

              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));

              session()->flash('message','password updated successfully');
              return redirect()->back();
            }

            else{
                  session()->flash('message','new password can not be the old password!');
                  return redirect()->back();
                }

        //    }

        //   else{
        //        session()->flash('message','old password doesnt matched ');
        //        return redirect()->back();
        //      }

       }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/admin/login');
    }

    public function user_logout()
    {
        Auth::guard('user')->logout();
        return redirect('/admin/login');
    }


    // Users
    public function users_list(){
        $title ="Users List";
        $Users = Users::where('user_type', 2)->get();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.users.users_list', compact('title', 'Users'));
    }

    public function new_user(){
        $title ="New Users";
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.users.new_user', compact('title'));
    }

    public function edit_itr($id){
        $title ="Edit ITR";
        $ItrTax = ItrTax::where('id', $id)->first();
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('Admin.itr.edit_itr', compact('title', 'ItrTax'));
    }

    public function store_user(Request $request){

        $Users = new Users();

        // $Users->user_id = $UserId;
        $Users->first_name = $request->first_name;
        $Users->last_name = $request->last_name;
        $Users->email = $request->email;
        $Users->password = Hash::make($request->password);

        $Users->user_type = 2;
        $Users->status = 0;
        $AddCustomers = $Users->save();


        return redirect()->back()->with('message','User Created Successfully');


    }
    public function changeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $Users = Users::find($request->id);
        $Users->status = $request->status;
        $Users->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
}
