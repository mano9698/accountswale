<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\FreeJobs;
use App\Models\UI\AppliedCandidates;
use App\Models\UI\Videos;
use App\Models\UI\Franchisee;

class HomeController extends Controller
{
    public function home(){
        $title = "::Welcome to Accountswale | Home::";

        // $GetUsers = json_decode(file_get_contents("http://freelancer.taprecruiter.com/api/freelancers/list/4"), true);

        // $Users = $GetUsers['data'];

        $GetQuestions = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/forum_list/2"), true);

        $Questions = $GetQuestions['data'];

        $FreeJobs = FreeJobs::where('status', 1)->get();

        $Videos = Videos::where('status', 1)->get();

        // $GetBlogs= json_decode(file_get_contents("http://accountswale.in/blog/wp-json/wp/v2/posts"), true);

        // echo json_encode($GetBlogs);
        // exit;

        return view('UI.layouts.home', compact('title', 'FreeJobs', 'Videos', 'Questions'));
    }

    public function become_partner(){
        $title = "::Welcome to Accountswale | Home::";


        // echo json_encode($Users);
        // exit;

        return view('UI.layouts.become_partner', compact('title'));
    }


    public function post_jobs(){
        $title = "::Welcome to Accountswale | Home::";


        // echo json_encode($Users);
        // exit;

        return view('UI.layouts.post_jobs', compact('title'));
    }

    public function AddFreeJobs(Request $request){
        $FreeJobs = new FreeJobs();

        $FreeJobs->jobs_id = rand(4, 7645);
        $FreeJobs->employer_name = $request->employer_name;
        $FreeJobs->job_title = $request->job_title;
        $FreeJobs->slug = strtolower(str_replace(' ', '_', $request->job_title));
        $FreeJobs->contact = $request->contact;
        $FreeJobs->email = $request->email;
        $FreeJobs->location = $request->location;
        $FreeJobs->work_from_home = $request->work_from_home;
        $FreeJobs->qualification = $request->qualification;
        $FreeJobs->experience = $request->experience;
        $FreeJobs->salary_budget = $request->salary_budget;
        $FreeJobs->job_description = $request->job_description;
        $FreeJobs->status = 0;

        $FreeJobs->save();

        return redirect()->back()->with('message','Job Posted Successfully');

    }


    public function apply_jobs(Request $request){
        $AppliedCandidates = new AppliedCandidates();

        $AppliedCandidates->job_id = $request->job_id;
        $AppliedCandidates->first_name = $request->first_name;
        $AppliedCandidates->last_name = $request->last_name;
        $AppliedCandidates->contact = $request->contact;
        $AppliedCandidates->email = $request->email;
        $AppliedCandidates->country = $request->country;
        $AppliedCandidates->state = $request->state;
        $AppliedCandidates->city = $request->city;
        $AppliedCandidates->area = $request->area;
        $AppliedCandidates->pincode = $request->pincode;

        if($request->hasfile('resume')){
            $extension = $request->file('resume')->getClientOriginalExtension();
            $dir = 'UI/resume/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('resume')->move($dir, $filename1);

            $AppliedCandidates->resume = $filename1;
        }

        $AppliedCandidates->save();

        return redirect()->back()->with('message','Job Posted Successfully');

    }

    public function AddFranchisee(Request $request){
        $Franchisee = new Franchisee();

        $Franchisee->first_name = $request->first_name;
        $Franchisee->last_name = $request->last_name;
        $Franchisee->email = $request->email;
        $Franchisee->mobile = $request->mobile;
        $Franchisee->country = $request->country;
        $Franchisee->state = $request->state;
        $Franchisee->city = $request->city;
        $Franchisee->pincode = $request->pincode;
        $Franchisee->area = $request->area;

        $Franchisee->save();

        return redirect()->back()->with('message','Thank you for your interest in Accountswale Franchise and submitting your details. Our team will talk to you soon.
        ');

    }
}
