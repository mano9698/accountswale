<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function blog_list(){
        $title = "Blogs List";

        // $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list"), true);

        $GetQuestions = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/forum_list/2"), true);

        $Questions = $GetQuestions['data'];


        // echo json_encode($Questions);
        // exit;

        return view('UI.blogs.list',compact('Questions', 'title'));
    }

    public function blog_details($id, $slug){
        $title = "Blog Details";

        $GetQuestions = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/forum_details/".$id), true);

        $Questions = $GetQuestions['data'];

        $GetComments = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/comments_list/".$id), true);

        $Comments = $GetComments['data'];

        $GetLikes = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/likes_list/".$id), true);

        $Likes = $GetLikes['data'];

        // echo json_encode($Questions);
        // exit;

        return view('UI.blogs.details',compact('Questions', 'Comments', 'Likes', 'title'));
    }
}
